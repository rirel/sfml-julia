
APPNAME = 'julia'
VERSION = '0.0.1'

def options(ctx):
	ctx.add_option('-d', '--debug', action='store_true', dest='debug')
	ctx.load('compiler_c compiler_cxx')

def configure(ctx):
	ctx.load('compiler_c compiler_cxx')
	
	# SFML
	ctx.env.LIB_SFML = ['sfml-system-s','sfml-window-s','sfml-graphics-s']
	ctx.env.LIBPATH_SFML = ['/Users/kuroki/SFML-2.3.2/build/lib']
	ctx.env.INCLUDES_SFML = ['/Users/kuroki/SFML-2.3.2/include']

	# JPEG
	ctx.env.LIB_JPEG = ['jpeg']
	ctx.env.LIBPATH_JPEG = ['/usr/local/Cellar/jpeg/8d/lib']
	ctx.env.INCLUDES_JPEG = ['/usr/local/Cellar/jpeg/8d/include']

	# FREETYPE
	ctx.env.LIB_FREETYPE = ['freetype']
	ctx.env.LIBPATH_FREETYPE = ['/usr/local/Cellar/freetype/2.6_1/lib']
	ctx.env.INCLUDES_FREETYPE = ['/usr/local/Cellar/freetype/2.6_1/include/freetype2']


	ctx.env.CXXFLAGS += ['-std=c++0x']

	if ctx.options.debug:
		ctx.env.append_value('CFLAGS', ['-g'])

def build(ctx):
	ctx.program(
		source = ['main.cpp'],
		includes = ['.'],
		use = ['SFML', 'JPEG', 'FREETYPE'],
		framework = ['CoreFoundation', 'OpenGL', 'AppKit', 'IOKit', 'Carbon'],
		target = 'main')
