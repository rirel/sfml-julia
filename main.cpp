#include <SFML/Graphics.hpp>

/* endpoint color 1 (dark blue) */
#define HUE1 240
#define SAT1 255
#define VAL1 102

/* endpoint color 2 (yellow orange) */
#define HUE2 30
#define SAT2 204
#define VAL2 255

/* graphics parameters */
#define HEIGHT 1200
#define WIDTH 1800
#define FLAGS 0
#define ITER 256

#define GOLDEN_RATIO 1.618033988749895

typedef struct { float i, r; } cplx;

sf::Color hsvWrapper(int hue, float sat, float val) {
	float f, p, q, t;
	hue %= 360;
	while (hue < 0) hue += 360;
	if (sat < 0.f) sat = 0.f;
	if (sat > 1.f) sat = 1.f;
	if (val < 0.f) val = 0.f;
	if (val > 1.f) val = 1.f;
	f = float(hue) / 60 - (hue / 60);
	p = val * (1.f - sat);
	q = val * (1.f - sat * f);
	t = val * (1.f - sat * (1-f));

	switch (hue / 60) {
		default: case 0: case 6:
			return sf::Color(val*255, t*255, p*255);
		case 1: return sf::Color(q*255, val*255, p*255);
		case 2: return sf::Color(p*255, val*255, t*255);
		case 3: return sf::Color(p*255, q*255, val*255);
		case 4: return sf::Color(t*255, p*255, val*255);
		case 5: return sf::Color(val*255, p*255, q*255);
	}
}

sf::Color palette(double value) {
	int hue;
	double sat, val;
	hue = (HUE2 - HUE1) * value + HUE1;
	sat = (SAT2 - SAT1) * value + SAT1;
	val = (VAL2 - VAL1) * value + VAL1;
	return hsvWrapper((int)hue, val, sat);
}

int main(int argc, char ** argv) {
	if (argc != 3) {
		fputs("\033[31mFATAL:\033[0m Expected argc = 3\n", stderr);
		return -1;
	}
	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT, FLAGS), "SFML Julia Set Renderer");
	sf::VertexArray plot;
	double zoom = 1, moveX = 0, moveY = 0;
	cplx c, n, o;

	/* constant c */	
	//c.r = -0.5;
	//c.r = -0.8;
	//c.i = 0.27015;
	//c.i = 0.156; 
	c.r = atof(argv[1]);
	c.i = atof(argv[2]);
	char *tbuf = (char *)malloc((strlen(argv[1]) + strlen(argv[2])) * sizeof(char) + (10 * sizeof(char)));
	sf::Font font; sf::Text text;
	if (!font.loadFromFile("scp-light.ttf")) return -1;
	text.setFont(font);
	text.setCharacterSize(48);
	text.setColor(sf::Color::White);
	strcat(tbuf, " c = ");
	strcat(tbuf, argv[1]);
	strcat(tbuf, " + ");
	strcat(tbuf, argv[2]);
	strcat(tbuf, "i");
	text.setString(tbuf);
	free(tbuf);


	for (int x = 0; x < WIDTH; x++)
	for (int y = 0; y < HEIGHT; y++) {
		sf::Vertex v;
		n.r = 1.5 * (x - WIDTH / 2) / (0.5 * zoom * WIDTH) + moveX;
		n.i = (y - HEIGHT / 2) / (0.5 * zoom * HEIGHT) + moveY;
		int i;
		for (i = 0; i < ITER; i++) {
			o = n;
			n.r = o.r * o.r - o.i * o.i + c.r;
			n.i = 2 * o.r * o.i + c.i;
			/* break if exceeds radius */
			if ((n.r * n.r + n.i * n.i) > 4) break;
		}
		//v.color = hsvWrapper((i + 5) % 256, 255, 255 * (i < ITER));
		v.color = palette((double)i/ITER);
		v.position = sf::Vector2f(x, y);
		plot.append(v);
	}
	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}
		window.clear();
		window.draw(plot);
		window.draw(text);
		window.display();
	}
}
	
