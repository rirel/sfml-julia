
-- SFML path
sfml = {
	libs = {
		"/Users/kuroki/SFML-2.3.2/build/lib",
		"/usr/local/Cellar/jpeg/8d/lib",
		"/usr/local/Cellar/freetype/2.6_1/lib"
	},
	includes = {
		"/Users/kuroki/SFML-2.3.2/include",
		"/usr/local/Cellar/jpeg/8d/include",
		"/usr/local/Cellar/freetype/2.6_1/include/freetype2"
	},
	flags = {
		"sfml-system-s",
		"sfml-window-s",
		"sfml-graphics-s",
		"jpeg",
		"freetype",
		"Carbon.framework",
		"AppKit.framework",
		"OpenGL.framework",
		"CoreFoundation.framework",
		"IOKit.framework"
	}
}


workspace "JuliaRenderer"
	configurations { "debug", "release" }

project "main"
	kind "ConsoleApp"
	language "C++"
	targetdir "build/%{cfg.buildcfg}"
	libdirs(sfml.libs)
	includedirs(sfml.includes)
	links(sfml.flags)

	files { "main.cpp" }

	filter "configurations:debug"
		defines { "DEBUG" }
		flags { "Symbols" }

	filter "configurations:release"
		optimize "On"

